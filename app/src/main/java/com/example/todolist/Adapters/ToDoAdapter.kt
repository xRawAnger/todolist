package com.example.todolist.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import android.widget.TextView
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.R
import com.example.todolist.database.data.RoomNote

class ToDoAdapter: ListAdapter<RoomNote, ViewHolder>(WordsComparator()) {

    private var tracker: SelectionTracker<Long>? = null

    fun setTracker(tracker: SelectionTracker<Long>?) {
        this.tracker = tracker
    }
    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long = position.toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = getItem(position)
        holder.title.text = current.title
        holder.creationDate.text = current.creationDate

        tracker?.let {
            if (it.isSelected(position.toLong())) {
                it.select(position.toLong())
                holder.marked.alpha = 1.0F
            } else {
                it.deselect(position.toLong())
                holder.marked.alpha = 0.0F
            }
        }
    }

}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {


    fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
        object : ItemDetailsLookup.ItemDetails<Long>() {
            override fun getPosition(): Int = adapterPosition
            override fun getSelectionKey(): Long = itemId
        }

    val title: TextView = itemView.findViewById(R.id.title)
    val creationDate: TextView = itemView.findViewById(R.id.creationDate)
    val marked: Button = itemView.findViewById(R.id.checkButton)

    companion object {
        fun create(parent: ViewGroup): ViewHolder {
            val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.todo_item, parent, false)
            return ViewHolder(view)
        }
    }


}


class WordsComparator : DiffUtil.ItemCallback<RoomNote>() {
    override fun areContentsTheSame(oldItem: RoomNote, newItem: RoomNote): Boolean {
        return oldItem.creationDate == newItem.creationDate
    }

    override fun areItemsTheSame(oldItem: RoomNote, newItem: RoomNote): Boolean {
        return oldItem == newItem
    }

}
package com.example.todolist.database.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.todolist.database.data.RoomNote


@Dao
interface NoteDao {
    @Query("select * from notes")
    fun getNotes() : LiveData<List<RoomNote>>

//    @Query("select * from notes where id = :id")
//    suspend fun getNoteById(id:Int) :RoomNote?

    @Delete
    suspend fun deleteNote(notes: RoomNote)

    @Insert
    suspend fun insertOrUpdateNote(notes: RoomNote)

    @Query("delete from notes where id = :id")
    suspend fun deleteNoteById(id: Int)
}
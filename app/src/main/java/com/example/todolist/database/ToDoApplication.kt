package com.example.todolist.database

import android.app.Application

class ToDoApplication : Application() {
    val database by lazy {

        ToDoListDatabase.getDatabase(this)
    }
    val repository by lazy {
        ToDoRepository(database.noteDao())
    }
}
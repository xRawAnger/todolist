package com.example.todolist.database

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.example.todolist.database.daos.NoteDao
import com.example.todolist.database.data.RoomNote

class ToDoRepository(private val noteDao: NoteDao) {

    val allNotes : LiveData<List<RoomNote>> = noteDao.getNotes()

    suspend fun insert(note: RoomNote){
        noteDao.insertOrUpdateNote(note)
    }

//    suspend fun getSingleNote(id: Int) {
//        noteDao.getNoteById(id)
//    }
    suspend fun deleteItem(note :RoomNote){
        noteDao.deleteNote(note)
    }
    suspend fun deleteSingleItem(id: Int){
        noteDao.deleteNoteById(id)
    }
}
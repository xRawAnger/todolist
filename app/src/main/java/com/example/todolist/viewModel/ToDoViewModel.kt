package com.example.todolist.viewModel

import androidx.lifecycle.*
import com.example.todolist.database.ToDoRepository
import com.example.todolist.database.data.RoomNote
import kotlinx.coroutines.launch

class ToDoViewModel(private val repository: ToDoRepository) :ViewModel(){

    //get all notes
    val allNotes : LiveData<List<RoomNote>> = repository.allNotes


    //add new item
    suspend fun insert(note: RoomNote)  = viewModelScope.launch{
        repository.insert(note)
    }

    //get single note
//    suspend fun getSingleNote(id: Int) = viewModelScope.launch{
//        repository.getSingleNote(id)
//    }


    suspend fun deleteMarkedItem(id: Int) = viewModelScope.launch {
        repository.deleteSingleItem(id)
    }

}

class ToDoViewModelFactory(private val repository: ToDoRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ToDoViewModel::class.java)) {

            return ToDoViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

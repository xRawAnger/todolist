package com.example.todolist.Activity.Auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.example.todolist.databinding.ActivityRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.lang.Exception

class RegisterActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var binding: ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        auth = Firebase.auth
        FirebaseAuth.getInstance()
        init()
    }

    private fun init() {
        binding.registerAlreadyRegisteredText.setOnClickListener() {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        binding.registerButton.setOnClickListener() {
            registerValidator()
        }
    }

    private fun registerValidator(){
        val email = binding.registerEmailEditText.text.toString()
        val pass = binding.registerPasswordEditText.text.toString()
        if (email.isEmpty() || pass.isEmpty()
        ) {
            Toast.makeText(this, "Fill all forms", Toast.LENGTH_SHORT).show()
        } else {
            myCoroutineForRegister()
            d("Evalidation1", "email is valid")

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    private fun myCoroutineForRegister() = runBlocking{
        registerWithEmail(
            firebaseAuth = FirebaseAuth.getInstance(),
        )
    }
    private suspend fun registerWithEmail(
        firebaseAuth: FirebaseAuth,
    ) = withContext(Dispatchers.IO){
        val email = binding.registerEmailEditText.text.toString()
        val pass = binding.registerPasswordEditText.text.toString()
        try {
            firebaseAuth.createUserWithEmailAndPassword(email, pass).await()
        } catch (e: Exception) {

        }
    }
}
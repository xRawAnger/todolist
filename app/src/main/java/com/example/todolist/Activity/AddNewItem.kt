package com.example.todolist.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.todolist.database.ToDoApplication
import com.example.todolist.database.data.RoomNote
import com.example.todolist.databinding.ActivityAddNewItemBinding
import com.example.todolist.viewModel.ToDoViewModel
import com.example.todolist.viewModel.ToDoViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*

class AddNewItem : AppCompatActivity() {
    private lateinit var binding: ActivityAddNewItemBinding

    private val listViewModel: ToDoViewModel by viewModels{
        ToDoViewModelFactory((application as ToDoApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddNewItemBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        binding.addNewItemButton.setOnClickListener {
            coroutineForInsert()
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
        }
    }

    private fun coroutineForInsert() {
        runBlocking {
            insertOperation()
        }
    }
    private suspend fun insertOperation()= withContext(Dispatchers.IO) {
        val content = binding.contentsEditText.text.toString()
        val title = binding.titleEditText.text.toString()
        val date = Calendar.getInstance().time
        if(content.isNotEmpty() || title.isNotEmpty()){
            listViewModel.insert(note = RoomNote("$date",title,content))
        }
    }
}
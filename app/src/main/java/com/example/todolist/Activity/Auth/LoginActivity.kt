package com.example.todolist.Activity.Auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.example.todolist.Activity.MainActivity
import com.example.todolist.databinding.ActivityLoginBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.lang.Exception

class LoginActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        auth = Firebase.auth
        FirebaseAuth.getInstance()
        init()
    }

    private fun init() {
        binding.loginButton.setOnClickListener {
            loginValidator()
        }
        binding.loginNotRegisteredText.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }
    private fun loginValidator(){
        val email = binding.loginEmailEditText.text.toString()
        val pass = binding.loginPasswordEditText.text.toString()
        if (
            email.isEmpty() || pass.isEmpty()
        ) {
            Toast.makeText(this, "Fill all forms!!!", Toast.LENGTH_SHORT).show()
        } else {
            loginWithEmailBasic(firebaseAuth = FirebaseAuth.getInstance())

//            myCoroutineForLogin()
            d("logging in", "Succesfuly logged in")
            Toast.makeText(this, "Login successful!!!", Toast.LENGTH_SHORT).show()

//            val intent = Intent(this, MainActivity::class.java)
//            startActivity(intent)
        }
    }
    private fun loginWithEmailBasic(firebaseAuth: FirebaseAuth){
        val email = binding.loginEmailEditText.text.toString()
        val pass = binding.loginPasswordEditText.text.toString()
        firebaseAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

//    private fun myCoroutineForLogin() = runBlocking {
//        loginWithEmail(
//            firebaseAuth = FirebaseAuth.getInstance())
//    }

//    private suspend fun loginWithEmail(
//        firebaseAuth: FirebaseAuth,
//    ) = withContext(Dispatchers.IO) {
//        val email = binding.loginEmailEditText.text.toString()
//        val pass = binding.loginPasswordEditText.text.toString()
//        try {
//            firebaseAuth.signInWithEmailAndPassword(email, pass).await()
//        } catch (e: Exception) {
//
//        }
//    }
}
package com.example.todolist.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import androidx.activity.viewModels
import androidx.recyclerview.selection.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.Adapters.ToDoAdapter
import com.example.todolist.Adapters.ViewHolder
import com.example.todolist.R
import com.example.todolist.database.ToDoApplication
import com.example.todolist.databinding.ActivityMainBinding
import com.example.todolist.viewModel.ToDoViewModel
import com.example.todolist.viewModel.ToDoViewModelFactory
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: ToDoAdapter
    private var tracker: SelectionTracker<Long>? = null
//    private lateinit var itemsSelected: MutableList<Long>
    var itemsSelected = mutableListOf<Long>()
    private val listViewModel:ToDoViewModel by viewModels{
        ToDoViewModelFactory((application as ToDoApplication).repository)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        listViewModel.allNotes.observe(this, { newData ->
            adapter.submitList(newData)
        })
        setSupportActionBar(binding.appBar)
        init()
        trackSelectedItems()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        binding.appBar.inflateMenu(R.menu.main_activity_toolbar_menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.addNewItem -> {
            val intent = Intent(this, AddNewItem::class.java)
            startActivity(intent)
            true
        }
        R.id.deleteItemButton -> {
            coroutineForDeletion()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }


    private fun init() {
        adapter = ToDoAdapter()
        binding.toDoRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.toDoRecyclerView.adapter = adapter

    }
    private fun trackSelectedItems() {
        tracker = SelectionTracker.Builder<Long>(
            "selection-1",
            binding.toDoRecyclerView,
            StableIdKeyProvider(binding.toDoRecyclerView),
            ItemLookup(binding.toDoRecyclerView),
            StorageStrategy.createLongStorage(),
        ).withSelectionPredicate(SelectionPredicates.createSelectAnything())
            .build()

        adapter.setTracker(tracker)


        tracker?.addObserver(object: SelectionTracker.SelectionObserver<Long>() {
            override fun onSelectionChanged() {
                itemsSelected.clear()
                itemsSelected.addAll(tracker!!.selection)
                d("itemsSelected","$itemsSelected")
            }

        })
    }


//    inner class ItemIdKeyProvider(private val recyclerView: RecyclerView)
//        : ItemKeyProvider<Long>(SCOPE_MAPPED) {
//
//        override fun getKey(position: Int): Long {
//            return adapter.getItemId(position)
//
//        }
//
//        override fun getPosition(key: Long): Int {
//            return RecyclerView.ViewHolder = li
//        }
//    }
    inner class ItemLookup(private val rv: RecyclerView)
        : ItemDetailsLookup<Long>() {
        override fun getItemDetails(event: MotionEvent)
                : ItemDetails<Long>? {
            val view = rv.findChildViewUnder(event.x, event.y)
            if(view != null) {
                val itemDetail = (rv.getChildViewHolder(view) as ViewHolder).getItemDetails()
                return itemDetail
            }
            return null
        }
    }

    private fun coroutineForDeletion() {
        runBlocking {
            deletionOperation()
        }
    }

    private suspend fun deletionOperation(){
        for (i in itemsSelected){
            listViewModel.deleteMarkedItem(i.toInt())
        }
    }

}


this is my practice app for "to do scheduling".

currently app starts with logging into your account(lets you in anyways for now even if you dont write correct info. doesnt matter if you randomly type in fields. if you use registered account,
it authenticates you and gives you your own token for later operations)

if you dont type anything it notfies you to write at least something in each field.

pressing on "not registered yet?" text will send you on registering page.

registering adds your account into firebase authentication users list.

you can add and delete tasks in app for now.

longpress triggers marking of items in recyclerview and after that you can click on items to mark multiple. currently ran into problem of showing which are marked and havent fixed it yet.

after clicking trash bin marker on top bar, your marked items will be deleted.

on pressing plus(+) button on topbar, you can add new item into recyclerview.